package com.example.game.model

import javax.persistence.*


@Entity
@Table(name = "game")
class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null
    var active = true
    var steep: Boolean? = true

//    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
//    @JoinTable(name = "user_id", joinColumns = [JoinColumn(name = "user_id")], inverseJoinColumns = [JoinColumn(name = "game_id")])
//    val gameUser: Collection<User>? = null

//    @OneToOne(mappedBy = "game")
//    var position: Position? = null

    @OneToOne
    @JoinColumn(name = "position_id")
    lateinit var position: Position

    @ManyToOne
    @JoinColumn(name = "one_user_id")
    lateinit var userOne: User

    @ManyToOne
    @JoinColumn(name = "two_user_id")
    var userTwo: User? = null

    @ManyToOne
    @JoinColumn(name = "winner_id")
    var winner: User? = null
}