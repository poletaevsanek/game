package com.example.game.model

//import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*


@Entity
@Table(name = "usr")
class User : UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
    private var username: String? = null
    private var password: String? = null
    var active = false

    @ElementCollection(targetClass = Role::class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = [JoinColumn(name = "user_id")])
    @Enumerated(EnumType.STRING)
    var roles: Set<Role?>? = null

//    @OneToMany(mappedBy = "oneUser")
//    var oneUser: Set<Game>? = null
//
//    @OneToMany(mappedBy = "twoUser")
//    var twoUser: Set<Game>? = null



    override fun getUsername(): String {
        return username!!
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return active
    }

    fun setUsername(username: String?) {
        this.username = username
    }

    override fun getAuthorities(): Set<Role?>? {
        return roles
    }

    override fun getPassword(): String {
        return password!!
    }

    fun setPassword(password: String?) {
        this.password = password
    }

}