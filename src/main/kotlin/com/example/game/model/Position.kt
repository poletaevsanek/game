package com.example.game.model

import javax.persistence.*

@Entity
@Table(name = "position")
class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null
    var one: Boolean? = null
    var two: Boolean? = null
    var three: Boolean? = null
    var four: Boolean? = null
    var five: Boolean? = null
    var six: Boolean? = null
    var seven: Boolean? = null
    var eight: Boolean? = null
    var nine: Boolean? = null
}