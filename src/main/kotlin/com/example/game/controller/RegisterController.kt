package com.example.game.controller

import com.example.game.model.Role
import com.example.game.model.User
import com.example.game.repository.UserRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/register")
class RegisterController {
    @Autowired
    private lateinit var userRepo: UserRepo

    @Autowired
    lateinit var encoder: PasswordEncoder

    @PostMapping()
    fun registration(@RequestBody user : User) : String {
        return if(!userRepo.existsByUsername(user.username.toString())){
            user.active = true
            user.setPassword(encoder.encode(user.password))
            user.roles = Collections.singleton(Role.USER)
            userRepo!!.save(user)
            "User ${user.username} created"
        }
        else {
            "User ${user.username} already exists"
        }
    }
}