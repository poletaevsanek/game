package com.example.game.controller


import com.example.game.model.Game
import com.example.game.model.Position
import com.example.game.model.User
import com.example.game.repository.GameRepo
import com.example.game.repository.PositionRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@RequestMapping("/game")
class GameController {
    @Autowired
    private lateinit var gameRepo: GameRepo

    @Autowired
    private lateinit var positionRepo: PositionRepo


    @GetMapping
    fun index (@AuthenticationPrincipal user: User): MutableList<Game?> {
        return gameRepo.findAllByActive(true)
    }


    @GetMapping("/my-game")
    fun myadmin(@AuthenticationPrincipal user: User): MutableList<Game?> {
        return gameRepo.findAllByUserOne(user)
    }

    @PostMapping
    fun create(
            @RequestBody position: Position,
            @AuthenticationPrincipal user: User
    ): ResponseEntity<Any>? {
        var userGame = gameRepo.findAllByUserOne(user)
        var idGame = 0
        idGame = checkGame(userGame)
        //Проверка только одной игры
        if(idGame != 0)
            return ResponseEntity("You have an unfinished game id $idGame", HttpStatus.BAD_REQUEST )
        userGame = gameRepo.findAllByUserTwo(user)
        idGame = checkGame(userGame)
        //Проверка только одной игры
        if(idGame != 0)
            return ResponseEntity("You have an unfinished game  id $idGame", HttpStatus.BAD_REQUEST )
        val elementPosition = positionRepo.save(position)
        val newGame = Game()
        if(position.one != null || position.two != null || position.three != null ||
                position.four != null || position.five != null || position.six != null ||
                position.seven != null || position.eight != null || position.nine != null)
            newGame.steep = false
        newGame.active = true
        newGame.userOne = user
        newGame.position = elementPosition
        gameRepo.save(newGame)
        return ResponseEntity.ok(newGame)
    }

    fun checkGame(game:  MutableList<Game?>): Int {
        for (item in game){
            if (item != null) {
                if(item.active)
                    return item.id!!.toInt()
            }
        }
        return 0
    }

    @PatchMapping("{id}")
    fun edit(
            @PathVariable id: Long,
            @RequestBody position: Position,
            @AuthenticationPrincipal user: User
    ): ResponseEntity<Any>? {
        val game: Game = gameRepo.findById(id).get()
        //Проверка активности игры
        if(!game.active)
            return ResponseEntity("Game over", HttpStatus.BAD_REQUEST)
        val positionGame = game.position
        var userGame = gameRepo.findAllByUserOne(user)
        var idGame = 0
        idGame = checkCurrentGamee(userGame, id)
        //Проверка только одной игры
        if(idGame != 0)
            return ResponseEntity("You have an unfinished game id $idGame", HttpStatus.BAD_REQUEST)
        userGame = gameRepo.findAllByUserTwo(user)
        idGame = checkCurrentGamee(userGame, id)
        //Проверка только одной игры
        if(idGame != 0)
            return ResponseEntity("You have an unfinished game  id $idGame", HttpStatus.BAD_REQUEST)

        if(game.userOne.id != user.id && game.userTwo == null){
            game.userTwo = user
            gameRepo.save(game)
        }

        if(game.userOne.id != user.id && game.userTwo?.id != user.id){
            return ResponseEntity("The room is full", HttpStatus.BAD_REQUEST )
        }

        if((game.userOne.id == user.id && !game.steep!!) || (game.userTwo?.id == user.id && game.steep!!)){
            return ResponseEntity("Opponent’s move", HttpStatus.BAD_REQUEST )
        }

        var move: Boolean = true // проверка меньше одной клетки
        //Проверка каждой кдетки. ЧТо бы не было перезаписей и был ход свой ход а не соперника.
        if(position.one != null && game.steep == position.one && positionGame.one == null){
            move = false
            positionGame.one = game.steep
        }
        if(position.two != null && game.steep == position.two && positionGame.two == null && move){
            move = false
            positionGame.two = game.steep
        }
        if(position.three != null && game.steep == position.three && positionGame.three == null && move){
            move = false
            positionGame.three = game.steep
        }
        if(position.four != null && game.steep == position.four && positionGame.four == null && move){
            move = false
            positionGame.four = game.steep
        }
        if(position.five != null && game.steep == position.five && positionGame.five == null && move){
            move = false
            positionGame.five = game.steep
        }
        if(position.six != null && game.steep == position.six && positionGame.six == null && move){
            move = false
            positionGame.six = game.steep
        }
        if(position.seven != null && game.steep == position.seven && positionGame.seven == null && move){
            move = false
            positionGame.seven = game.steep
        }
        if(position.eight != null && game.steep == position.eight && positionGame.eight == null && move){
            move = false
            positionGame.eight = game.steep
        }
        if(position.nine != null && game.steep == position.nine && positionGame.nine == null && move){
            move = false
            positionGame.nine = game.steep
        }
        if(!move)
            game.steep = !game.steep!!
        positionRepo.save(positionGame)
        // Проверка окончания игры (Костыль конечно)
        if(     (positionGame.one === positionGame.two &&  positionGame.two === positionGame.three && positionGame.one != null) ||
                (positionGame.four === positionGame.five &&  positionGame.five === positionGame.six  && positionGame.four != null) ||
                (positionGame.seven === positionGame.eight && positionGame.eight === positionGame.nine  && positionGame.seven != null) ||
                (positionGame.one === positionGame.four && positionGame.four === positionGame.seven  && positionGame.one != null) ||
                (positionGame.two === positionGame.five && positionGame.five === positionGame.eight  && positionGame.two != null) ||
                (positionGame.three === positionGame.six && positionGame.six === positionGame.nine  && positionGame.three != null) ||
                (positionGame.one === positionGame.five && positionGame.five === positionGame.nine  && positionGame.one != null) ||
                (positionGame.three === positionGame.five && positionGame.five === positionGame.seven  && positionGame.three != null)
        ){
            game.winner = user
            game.active = false
        }


        gameRepo.save(game)
        if(!game.active){
            val winner = game.winner?.id
            return ResponseEntity("Game over  winner $winner", HttpStatus.BAD_REQUEST)
        }
        return  ResponseEntity.ok(game)
    }

    fun checkCurrentGamee(game:  MutableList<Game?>, id: Long): Int {
        for (item in game){
            if (item != null) {
                if(item.active && item.id != id)
                    return item.id!!.toInt()
            }
        }
        return 0
    }
}