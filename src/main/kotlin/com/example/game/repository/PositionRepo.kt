package com.example.game.repository

import com.example.game.model.Position
import org.springframework.data.jpa.repository.JpaRepository

interface PositionRepo : JpaRepository<Position?, Long?> {
}