package com.example.game.repository

import com.example.game.model.Game
import com.example.game.model.User
import org.springframework.data.jpa.repository.JpaRepository

interface GameRepo: JpaRepository<Game?, Long?> {
    fun findAllByUserOne(userOne: User?): MutableList<Game?>
    fun findAllByUserTwo(userOne: User?): MutableList<Game?>
    fun findAllByActive(active: Boolean): MutableList<Game?>
}